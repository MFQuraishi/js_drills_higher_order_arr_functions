function find(arr, fun){

    if(!Array.isArray(arr)){
        console.error("argument not an array");
        return "insert an array";
    }

    for(let i=0; i<arr.length; i++){
        if(fun(arr[i])){
            return arr[i];
        }
    }
    return 'undefined';
}

module.exports={
    find: find,
}