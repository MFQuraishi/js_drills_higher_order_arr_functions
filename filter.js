function filter(arr, fun){

    if(!Array.isArray(arr)){
        console.error("argument not an array");
        return "insert an array";
    }

    let temp = [];
    for(let i=0; i<arr.length; i++){
        if(fun(arr[i])){
            temp.push(arr[i]);
        }
    }
    return temp;
}

module.exports = {
    filter: filter,
}