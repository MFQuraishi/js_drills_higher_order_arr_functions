function reduce(arr, fun, start){

    if(!Array.isArray(arr)){
        console.error("argument not an array");
        return "insert an array";
    }

    let i = 0;
    if(typeof start !== typeof 1){
        start = arr[0];
        i=1;
    }

    for(i; i<arr.length; i++){
        start = fun(start, arr[i])
    }

    return start;

}

module.exports = {
    reduce: reduce
}