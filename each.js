function each(arr, fun){

    if(!Array.isArray(arr)){
        console.error("argument not an array");
        return "insert an array";
    }

    for(let i=0; i<arr.length; i++){
        fun(arr[i]);
    }
}

module.exports = {
    each: each
}