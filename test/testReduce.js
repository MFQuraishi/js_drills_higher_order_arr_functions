// to find sum of squares of elements

const { reduce } = require('./../reduce.js');
const items = [1, 2, 3, 4, 5];

let fun = function (st, e) {
    return e * st;
}

console.log(reduce(items, fun, 1));

// or

// console.log(reduce(items, (st, e) => st + e*e, 5));