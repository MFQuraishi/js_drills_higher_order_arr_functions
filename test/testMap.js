const {map} = require('./../map.js');

const items = [1, 2, 3, 4, 5, 5];

// const items = 'undefined';

// ***********************
let fun = (i)=>{
    return i*i;
}

console.log(map(items, fun));
//***************************

//or

console.log(map(items, i => i*i));