const {each} = require('./../each.js');

const items = [1, 2, 3, 4, 5, 5];
// const items = 'undefined';
sum=0;

let print = (i) => {
    console.log(i);
    sum+=i;
}

each(items, print);
console.log(sum);