function flatten(arr){

    if(!Array.isArray(arr)){
        console.error("argument not an array");
        return "insert an array";
    }

    let temp = [];
    function recursive(arr){
        for(let i=0; i<arr.length; i++){
            if(typeof(arr[i]) === typeof([])){
                recursive(arr[i]);
            }
            else{
                temp.push(arr[i]);
            }
        }
    }

    recursive(arr);
    return temp;
}

module.exports = {
    flatten: flatten
}