function map(arr, fun){

    if(!Array.isArray(arr)){
        console.error("argument not an array");
        return "insert an array";
    }

    let temp = [];
    for(let i=0; i<arr.length; i++){
        temp.push(fun(arr[i]));
    }

    return temp;
}

module.exports = {
    map:map,
}